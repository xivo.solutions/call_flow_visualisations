import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `<h1>Flux inter-mediaserver :</h1>
  <app-graphe-call-flow></app-graphe-call-flow>`,
  styleUrls: ['./app.component.css'],

})
export class AppComponent {
  title = 'default';

}
