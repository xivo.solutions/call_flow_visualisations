import { Component, Input } from '@angular/core';
import { Import } from '../import.model';
import { GrapheBuilderService } from '../graphe-builder.service';

@Component({
  selector: 'app-sub-component-graphe',
  templateUrl: './sub-component-graphe.component.html',
  styleUrls: ['./sub-component-graphe.component.css']
})
export class SubComponentGrapheComponent {

  private _data : Import | null = null;
  private _graphId : String = "myDiagramDiv"
  @Input()
  set data(data : Import | null){

    this._data = data;
    if (this._data !== null){
      this._grapheBuilderService.buildGraph(this._data, this._graphId);
    }

  }

constructor(private _grapheBuilderService : GrapheBuilderService){}

}
