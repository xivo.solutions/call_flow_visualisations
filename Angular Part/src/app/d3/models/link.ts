import { Node } from './';
import  * as d3 from "d3";

// Implementing SimulationLinkDatum interface into our custom Link class
export class Link implements d3.SimulationLinkDatum<Node> {
    // Must - defining enforced implementation properties
    source!: Node;
    target!: Node;
    color : string;
    size : number;
    id : number;
    text : string;
    nbCalls : number;

    constructor(id : number,nodes : Node[], source: string | number, target: string | number, color : string, size : number, text : string, nbCalls : number) {

        this.id = id;
        try {
          // try to retrieve the source node
          let sourceNode : Node | undefined
          if (typeof source === "string")
            sourceNode = nodes.find((node : Node) => node.id == source);
          else
            sourceNode = nodes[source];

          // check if it was retrieved
          if (typeof sourceNode === 'undefined'){
            throw(`Error, source node corresponding to identifier ${source} not found`);
          }
          else {
            this.source = sourceNode
          }

          // try to retrieve the target node
          let targetNode : Node | undefined
          if (typeof target === "string") {
            targetNode = nodes.find((node : Node) => node.id == target);
          }
          else {
            targetNode = nodes[target];
          }
          // check if it was retrieved
          if (typeof targetNode === 'undefined') {
            throw(`Error, target node corresponding to identifier ${target} not found`);
          }
          else {
            this.target = targetNode
          }
        }
        catch(error) {console.error(error);}

        this.size = size;
        this.color = color;
        this.text = text;
        this.nbCalls = nbCalls;
    }
}
