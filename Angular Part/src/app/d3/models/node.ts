import * as d3 from 'd3';

// Implementing SimulationNodeDatum interface into our custom Node class
export class Node implements d3.SimulationNodeDatum {
  // Optional - defining optional implementation properties - required for relevant typing ;
  x?: number;
  y?: number;
  vx?: number;
  vy?: number;
  fx?: number | null;
  fy?: number | null;

  id: string;
  size: any = 0;
  color : string;
  shape : string;
  users : number;

  constructor(id : string, shape : string, color : string, size : any, users : number) {
      this.id = id;
      this.shape = shape;
      this.color = color;
      this.size = size;
      this.users = users
  }
}
