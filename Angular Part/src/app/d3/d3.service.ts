import { Injectable } from '@angular/core';
import { Link } from './models/link';
import { ForceDirectedGraph } from './models/force-directed-graph';
import { Node } from './models/node';
import * as d3 from 'd3';

@Injectable({
  providedIn: 'root'
})
export class D3Service {

  constructor() {}

  /** A method to bind a pan and zoom behaviour to an svg element */
  applyZoomableBehaviour(svgElement : any, containerElement : any) {
    let svg, container : any, zoomed, zoom;

    svg = d3.select(svgElement);
    container = d3.select(containerElement);

    zoomed = (event : any) => {
      const transform = event.transform;
      container.attr('transform', 'translate(' + transform.x + ',' + transform.y + ') scale(' + transform.k + ')');
    }

    zoom = d3.zoom().on('zoom', zoomed);
    svg.call(zoom);
  }

/** A method to bind a draggable behaviour to an svg element */
applyDraggableBehaviour(element : any, node: Node, graph: ForceDirectedGraph) {
  const d3element = d3.select(element);

  function started(event : any) {
    /** Preventing propagation of dragstart to parent elements */
    event.sourceEvent.stopPropagation();

    if (!event.active) {
      graph?.simulation?.alphaTarget(0.3).restart();
    }

    event.on('drag', dragged).on('end', ended);

    function dragged(event : any) {
      node.fx = event.x;
      node.fy = event.y;
    }

    function ended(event : any) {
      if (!event.active) {
        graph?.simulation?.alphaTarget(0);
      }

      node.fx = null;
      node.fy = null;
    }
  }

  d3element.call(d3.drag()
    .on('start', started));
}

    /** The interactable graph we will simulate in this article
    * This method does not interact with the document, purely physical calculations with d3
    */
    getForceDirectedGraph(nodes: Node[], links: Link[], options: { width : number, height : number} ) {
      let graph = new ForceDirectedGraph(nodes, links, options);
      return graph;
    }
}
