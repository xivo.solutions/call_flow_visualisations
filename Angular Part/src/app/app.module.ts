import { CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GraphComponent } from './visuals/graph/graph.component';
import { NodeVisualComponent } from './visuals/shared/node-visual/node-visual.component';
import { LinkVisualComponent } from './visuals/shared/link-visual/link-visual.component';
import { D3Service } from './d3';
import { SHARED_VISUALS } from './visuals/shared';
import { D3_DIRECTIVES } from './d3/directives';
import { DataImportService } from './API_call/data-import.service';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePickerComponent } from "./visuals/shared/date-picker/date-picker.component";
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import {LegendComponent} from './visuals/legend/legend.component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MatSliderModule} from "@angular/material/slider";

registerLocaleData(localeFr, 'fr');

@NgModule({
    declarations: [
        AppComponent,
        GraphComponent,
        LegendComponent,
        NodeVisualComponent,
        LinkVisualComponent,
        ...SHARED_VISUALS,
        ...D3_DIRECTIVES,
    ],
    providers: [
        { provide: LOCALE_ID, useValue: 'fr'},
        D3Service,
        DataImportService,
        LegendComponent
    ],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        DatePickerComponent,
        CommonModule,
        FormsModule,
        MatSliderModule,
    ],
    schemas: [
      CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class AppModule { }
