export interface Calls {
  caller_mds: string;
  callee_mds: string;
  calls: number;
}
