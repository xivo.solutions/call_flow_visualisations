import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, map , of} from 'rxjs';
import { Import } from './models';

@Injectable({
  providedIn: 'root'
})
export class DataImportService {

  private _hostName: string = 'http://localhost:9000';
  private _import$ = new BehaviorSubject<Import | undefined>(undefined);

  private _maxUsers$ = new BehaviorSubject<number>(0);
  private _maxNodeCalls$ = new BehaviorSubject<number>(0);
  private _maxLinkCalls$ = new BehaviorSubject<number>(0);


  constructor(private _httpClient: HttpClient) {
  }


  requestDate(date1 : string, date2 : string) {
    this._httpClient.post<Import>(`${this._hostName}/CallFlows`, {lowerBound : date1, upperBound : date2}).subscribe((data : Import | undefined) => {
      if (data){
        this._import$.next(data)

        const maxUsers = Math.max.apply(null, data.mdsList.map( mds => mds.users));
        this._maxUsers$.next(maxUsers);

        const mdsMaxCalls = Math.max.apply(null, data.mdsList.map( mds => mds.calls));
        const trunkMaxCalls = Math.max.apply(null, data.trunkList.map( trunk => trunk.incoming + trunk.outgoing));
        const maxNodeCalls =  Math.max.apply(null, [trunkMaxCalls, mdsMaxCalls].concat([0]));
        this._maxNodeCalls$.next(maxNodeCalls)

        const maxCallFromTrunk = Math.max.apply(null, data.trunkList.map( trunk => [trunk.incoming, trunk.outgoing]).flat());
        const maxCallFromMds = Math.max.apply(null, data.callDatasList.map(x=>x.calls));
        const maxLinkCalls = Math.max.apply(null, [maxCallFromMds, maxCallFromTrunk].concat([0]));
        this._maxLinkCalls$.next(maxLinkCalls)

      }
    })
  }

  subscribeToImport(): Observable<Import | undefined> {
    return this._import$.asObservable();
  }

  subscribeToMaxUsers() : Observable<number> {
    return this._maxUsers$.asObservable();
  }

  subscribeToMaxNodeCalls() : Observable<number> {
    return this._maxNodeCalls$.asObservable();
  }

  subscribeToMaxLinkCalls() : Observable<number> {
    return this._maxLinkCalls$.asObservable();
  }
}

