import { SHARED_VISUALS } from './../index';
import { Component, Input } from '@angular/core';
import { Node } from "../../../d3/models/node"
import { LegendService } from 'app/visuals/legend/legend.service';
@Component({
  selector: '[nodeVisual]',
  templateUrl : './node-visual.component.html',
  styleUrls: ['./node-visual.component.scss']
})
export class NodeVisualComponent {
  @Input('nodeVisual') node: Node;

  constructor(private _legendService : LegendService) { }

  get x() {
    if (this.node && this.node.x && this.node.shape == "rectangle") {
      return this.node!.x-this.node.size.x/2
    }
    if (this.node && this.node.x && this.node.shape == "circle") {
      return this.node!.x
    }
    return 0
  }

  get y() {
    if (this.node && this.node.y && this.node.shape == "rectangle") {
      return this.node!.y-this.node.size.y/2
    }
    if (this.node && this.node.x && this.node.shape == "circle") {
      return this.node!.y
    }
    return 0
  }

  get display() : Boolean{
    return this.node.users <= this._legendService.maxNode && this.node.users >= this._legendService.minNode
  }
}
