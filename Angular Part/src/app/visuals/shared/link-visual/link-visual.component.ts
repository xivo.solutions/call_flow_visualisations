import { Component, Input } from '@angular/core';
import { Link } from 'app/d3';
import * as d3 from 'd3';
@Component({
  selector: '[linkVisual]',
  templateUrl : './link-visual.component.html',
  styleUrls: ['./link-visual.component.scss']
})
export class LinkVisualComponent  {
  @Input('linkVisual') link : Link;

  _y1 : number = 0
  _x1 : number = 0
  _y2 : number = 0
  _x2 : number = 0

  // coordinates of the source / target of the arrow
  y1 : number = 0
  x1 : number = 0
  y2 : number = 0
  x2 : number = 0

  gap = 0.35

  ngOnInit() {
    const markerId = `arrowhead_${this.link.id}`;
    const existingMarker = d3.select(`defs #${markerId}`);

    if (existingMarker.node()) {
      // If the marker with the same ID exists, remove it
      existingMarker.remove();
    }

    d3.select("defs")
      .append("svg:marker")
      .attr("id", markerId)
      .attr("viewBox", "0 -5 10 10")
      .attr("refX", 6)
      .attr("orient", "auto")
      .append("svg:path")
      .attr("d", "M0,-5L10,0L0,5")
      .style("fill", this.link.color);
  }

  initSourceNodePosition(){
    if (this.link && this.link.source && this.link.source.x && this.link.source.y) {
      this._x1 = this.link.source.x
      this._y1 = this.link.source.y
    }
  }

  initTargetNodePosition(){
    if (this.link && this.link.target && this.link.target.x && this.link.target.y) {
      this._x2 = this.link.target.x
      this._y2 = this.link.target.y
    }
  }

  selfLinkPosition(r : number) {
    this.x1 = this._x1
    this.y1 = this._y1 - r;
    this.x2 = this._x1 + r;
    this.y2 = this._y1
  }

  get alpha() {
    return Math.atan2(this._y2-this._y1, this._x2-this._x1);
  }

  setTargetLinkPosition (shape : string, linkSize : number, nodeSize : any) {
    if (shape=='circle') {
      const size = linkSize + nodeSize;
      this.x2 = this._x2 - size * Math.cos(this.alpha + this.gap);
      this.y2 = this._y2 - size * Math.sin(this.alpha + this.gap);
    }
    if (shape=='rectangle'){
      const sgn = Math.sign(this._y2-this._y1)
      this.x2 = this._x2 + sgn * nodeSize.x/4
      this.y2 = this._y2 - sgn * nodeSize.y/2
    }
  }

  setSourceLinkPosition (shape : string, size : any) {
    if (shape == 'circle') {
      this.x1 = this._x1 + size * Math.cos(this.alpha - this.gap);
      this.y1 = this._y1 + size * Math.sin(this.alpha - this.gap);
    }

    if (shape=='rectangle'){
      var sgn = Math.sign(this._y2-this._y1)
      this.x1 = this._x1 + sgn * size.x/4
      this.y1 = this._y1 + sgn * size.y/2
    }
  }


  constructPath(self : Boolean, drx : number , dry : number): string {
    if (self){
      return `M${this.x1},${this.y1}A${drx},${dry} 0,1,1 ${this.x2},${this.y2}`;}
    else{
      return `M${this.x1},${this.y1}A0,0 0,0,0 ${this.x2},${this.y2}`;}
  }

  get path() : string {
    var drx = 0;
    var dry = 0;
    var selfLinked : Boolean
    this.initSourceNodePosition();
    this.initTargetNodePosition();


    if (this.link.target == this.link.source) {
      this.selfLinkPosition(this.link.target.size)
      drx = dry = this.link.size*3.5;
      selfLinked = true;
    }
    else {
      this.setSourceLinkPosition(this.link.source.shape, this.link.source.size);
      this.setTargetLinkPosition(this.link.target.shape, this.link.size, this.link.target.size);
      selfLinked = false;
    }
    return this.constructPath(selfLinked, drx, dry)
  }




  get arrowHeadMarkerName() : string {
    return `url(#arrowhead_${this.link.id})`
  }

  public get unitaryNormalVector() {
    function normalize(vect : {x : number, y : number}) {
      return {x : vect.x/(Math.sqrt(vect.x**2 + vect.y**2)), y : vect.y/(Math.sqrt(vect.x**2 + vect.y**2))};
    }
    return normalize({x : (this.y1-this.y2),y : (this.x2-this.x1)});
  }

  get textCoordinates() : {x : number, y : number} {
    var x : number = (this.x1+this.x2)/2;
    var y : number = (this.y1+this.y2)/2;
    var offset_x = - this.unitaryNormalVector.x * 25
    var offset_y = - this.unitaryNormalVector.y * 25
    return { x : x + offset_x, y : y + offset_y}
  }
}
