import { ChangeDetectorRef, Component, HostListener, Input } from '@angular/core';
import { DataImportService } from 'app/API_call/data-import.service';
import {interpolateRgb, piecewise} from "d3"
import { Import } from 'app/API_call/models';
import { Link } from 'app/d3/models/link';
import { Node } from 'app/d3/models/node';
import { ForceDirectedGraph } from 'app/d3/models/force-directed-graph';
import { D3Service } from 'app/d3/d3.service';
import { LegendComponent } from '../legend/legend.component';


@Component({
  selector: 'graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})

export class GraphComponent {
  nodes: Node[] = [];
  links: Link[] = [];

  private get MIN_NODE_SIZE() {return 20};
  private get MAX_NODE_SIZE() {return 80};
  private get MAX_LINK_SIZE() {return 25};


  graph!: ForceDirectedGraph;

  @HostListener('window:resize', ['$event'])
  onResize(event : any) {
    if ( this.graph )
      this.graph.initSimulation(this.options);
  }



  constructor(private d3Service: D3Service, private ref: ChangeDetectorRef, private _dataImportService : DataImportService, private _legendComponent : LegendComponent) {  }


  ngOnInit() {
    this._dataImportService.subscribeToImport().subscribe((data : Import | undefined) => {
      if (data) {

        this._dataImportService.subscribeToMaxNodeCalls().subscribe(maxNodeCalls => {
          this._dataImportService.subscribeToMaxLinkCalls().subscribe(maxLinkCalls => {
            this._dataImportService.subscribeToMaxUsers().subscribe(maxUsers => {

            this.nodes=[]
            this.links=[]

            data.mdsList.forEach(element => {
              this.nodes.push(new Node(element.name, 'circle', this.nodeColorFromCalls(element.calls, maxNodeCalls),this.nodeSizeFromUsers(element.users, maxUsers), element.users));
            });

            data.callDatasList.forEach((element, i) => {
              this.links.push(new Link(i, this.nodes, element.caller_mds, element.callee_mds, this.linkColorFromCalls(element.calls, maxLinkCalls), this.linkSizeFromCalls(element.calls, maxLinkCalls), element.calls.toString(), element.calls));
            });

            data.trunkList.forEach((element, i) => {
              i+=data.callDatasList.length
              this.nodes.push(new Node(element.name, 'rectangle', this.nodeColorFromCalls(element.incoming + element.outgoing, maxNodeCalls) , {x : element.name.length*12, y : 45}, 0));
              if (element.incoming!=0) {
                this.links.push(new Link(i, this.nodes, element.mds, element.name, this.linkColorFromCalls(element.incoming, maxLinkCalls), this.linkSizeFromCalls(element.incoming, maxLinkCalls), element.incoming.toString(), element.incoming))
              }

              if (element.outgoing!=0) {
                this.links.push(new Link(i, this.nodes, element.name, element.mds, this.linkColorFromCalls(element.outgoing, maxLinkCalls), this.linkSizeFromCalls(element.outgoing, maxLinkCalls), element.outgoing.toString(), element.outgoing))
              }
            });
            /** Receiving an initialized simulated graph from our custom d3 service */


            this.graph = this.d3Service.getForceDirectedGraph(this.nodes, this.links, this.options);

            /** Binding change detection check on each tick
             * This along with an onPush change detection strategy should enforce checking only when relevant!
             * This improves scripting computation duration in a couple of tests I've made, consistently.
             * Also, it makes sense to avoid unnecessary checks when we are dealing only with simulations data binding.
             */
            this.graph.ticker.subscribe((d) => {
              this.ref.markForCheck();
            });
          })
        })
      })
    }
  })
}

  ngAfterViewInit() {
    if (this.graph) {
      this.graph.initSimulation(this.options);
    }

  }

  get options() {
    return {
      width: window.innerWidth-332,
      height: window.innerHeight
    };
  }

  private getVal(val : number, maxVal : number){
    if (maxVal == 0) return 0
    else return val/maxVal
  }

  private nodeColorFromCalls(value : number, maxValue : number) : string {
    const interpolate = piecewise(interpolateRgb.gamma(2.2), ["#2dc937", "#99c140", "#e7b416", "#db7b2b", "#cc3232"]);
    return interpolate(this.getVal(value, maxValue))
  }

  private nodeSizeFromUsers(value : number, maxValue : number) : number {
    return Math.max(this.getVal(value, maxValue)*this.MAX_NODE_SIZE, this.MIN_NODE_SIZE)
  }

  private linkSizeFromCalls(value : number, maxValue : number) : number {
    return this.getVal(value, maxValue)*this.MAX_LINK_SIZE;
  }

  private linkColorFromCalls(value : number, maxValue : number) : string {
    const interpolate = piecewise(interpolateRgb.gamma(1), ["#2dc937", "#99c140", "#e7b416", "#db7b2b", "#cc3232"])
    return interpolate(this.getVal(value, maxValue))
  }



  // displayLink(link : Link): boolean{
  //   return this.minNode <= link.source.users && this.maxNode >= link.source.users && this.minNode <= link.target.users && this.maxNode >= link.target.users;
  // }

}
