import { Injectable } from '@angular/core';
import { LegendComponent } from './legend.component';
import { DataImportService } from 'app/API_call/data-import.service';

@Injectable({
  providedIn: 'root'
})
export class LegendService {
  maxValueNode : number;
  maxValueLink : number;
  minValueNode = 0;
  minValueLink = 0;

  minNode : number;
  minLink : number;
  maxNode : number;
  maxLink : number;

  constructor(private _dataImportservice : DataImportService) {
    this._dataImportservice.subscribeToMaxUsers().subscribe( value => {
      this.maxValueNode = value;
      this.maxNode = value;
      this.minNode = 0;
    });

    this._dataImportservice.subscribeToMaxLinkCalls().subscribe( value=> {
      this.maxValueLink = value;
      this.maxLink = value;
      this.minLink = 0;
    });
  }
}
