import { Component } from '@angular/core';
import { DataImportService } from 'app/API_call/data-import.service';
import { LegendService } from './legend.service';

@Component({
    selector: 'app-legend',
    templateUrl : './legend.component.html',
    styleUrls: ['./legend.component.scss']
})
export class LegendComponent{

  constructor(private _legendService : LegendService) {}

    formatLabel(value: number): string {

        if (value >= 1000) {
            return Math.round(value / 1000) + 'k';
        }
        return value.toString();
    }

    get maxValueNode() : number {
      return this._legendService.maxValueNode
    }

    get maxValueLink() : number {
      return this._legendService.maxValueNode
    }

    get minValueNode(){
      return this._legendService.minValueNode
    }

    get minValueLink(){
      return this._legendService.minValueLink
    }

    set minNode(value : number) {
      this._legendService.minNode = value
    }
    set minLink(value : number) {
      this._legendService.minLink = value
    }
    set maxNode(value : number) {
      this._legendService.maxNode = value
    }
    set maxLink(value : number) {
      this._legendService.maxLink = value
    }

}
