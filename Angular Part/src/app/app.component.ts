import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})



export class AppComponent {
  title: any;
  mdsMaxUsers : number = 10;
  linkMaxCalls : number = 10;
}
